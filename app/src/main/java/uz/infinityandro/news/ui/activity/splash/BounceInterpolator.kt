package uz.infinityandro.worldnews.ui.activity.splash

import android.view.animation.Interpolator

class BounceInterpolator:Interpolator {
    private var mAmplitude = 1.0
    private var mFrequency = 10.0

    constructor(mAmplitude: Double, mFrequency: Double) {
        this.mAmplitude = mAmplitude
        this.mFrequency = mFrequency
    }

    override fun getInterpolation(input: Float): Float {
        return ((-1 * Math.pow(Math.E, -input/ mAmplitude) *
                Math.cos(mFrequency * input) + 1)).toFloat()
    }
}