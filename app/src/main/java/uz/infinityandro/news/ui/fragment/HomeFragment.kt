package uz.infinityandro.worldnews.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.news.R
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.news.databinding.HomeScreenBinding
import uz.infinityandro.news.presenter.room.NewsDatabase
import uz.infinityandro.news.ui.activity.main.NewsActivity
import uz.infinityandro.news.ui.adapter.GetDaoAdapter
import uz.infinityandro.worldnews.data.model.ArticlesItem
import uz.infinityandro.worldnews.presenter.viewmodel.Impl.NewsViewModelImpl
import uz.infinityandro.worldnews.ui.adapter.HomeRecyclerView
import uz.infinityandro.worldnews.utils.Constants
import uz.infinityandro.worldnews.utils.isConnected
import uz.infinityandro.worldnews.utils.showToast

class HomeFragment : Fragment(R.layout.home_screen) {
    private val binding by viewBinding(HomeScreenBinding::bind)
    private val viewModel: NewsViewModelImpl by viewModel()
    private lateinit var adapter: HomeRecyclerView
    private lateinit var adapter1:GetDaoAdapter
    private lateinit var databse:NewsDatabase
    var list = ArrayList<ArticlesItem>()
    var list1=ArrayList<Article>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        adapterSet()
        databse= NewsDatabase.getInstance(requireContext())
        if (isConnected()){
        observers()
        loadNews()
        }else{
            loadDataNews()
            loadDataObservers()
        }

    }



    private fun loadDataNews() {
    }

    private fun loadDataObservers() {
        CoroutineScope(Dispatchers.IO).launch {
            val user=databse.newsDao().getNews()
            list1.addAll(user)
            adapter1.notifyDataSetChanged()
            if (!list1.isNullOrEmpty()){
                binding.progressBarHome.visibility=View.GONE
            }

        }


    }

    private fun loadNews() {
        viewModel.getAllBooks(requireContext(),Constants.COUNTRY, Constants.PAGE, Constants.RANDOM_NEWS, Constants.ACCESS_KEY)
    }

    private fun observers() {
        viewModel.errorMessageLiveData.observe(requireActivity(), {
            showToast(it)
        })
        viewModel.progressLiveData.observe(requireActivity(), {
            if (it) {
                binding.progressBarHome.visibility = View.VISIBLE
            } else {
                binding.progressBarHome.visibility = View.GONE

            }
        })
        viewModel.allDataLiveData.observe(requireActivity(), {news->
            if (!news.articles.isNullOrEmpty()) {
                list.addAll(news.articles)
                adapter.notifyDataSetChanged()
                news.articles.forEach {
                    list1.add(Article(0,it.publishedAt,it.author,it.urlToImage,it.description,it.title,it.url,it.content,it.source?.name))
                }
               CoroutineScope(Dispatchers.IO).launch {databse.newsDao().insert(list1)}


            }
        })

    }

    private fun adapterSet() {
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        binding.recyclerView.setHasFixedSize(true)
        if (isConnected()){


        adapter = HomeRecyclerView(requireContext(), list) {news->
            Intent(requireContext(), NewsActivity::class.java).let {
                it.putExtra("new",news)
                startActivity(it)
            }
        }
            binding.recyclerView.adapter = adapter

        }else{
            adapter1 = GetDaoAdapter(requireContext(), list1) {news->
                Intent(requireContext(), NewsActivity::class.java).let {
                    it.putExtra("new",news)
                    startActivity(it)
                }
            }
            binding.recyclerView.adapter = adapter1

        }

    }
}