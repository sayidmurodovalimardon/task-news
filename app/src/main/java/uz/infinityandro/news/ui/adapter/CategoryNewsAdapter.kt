package uz.infinityandro.news.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uz.infinityandro.news.R
import uz.infinityandro.news.databinding.ItemCategoryNewsRecyclerBinding
import uz.infinityandro.worldnews.data.model.ArticlesItem
import java.text.SimpleDateFormat
import java.util.*

class CategoryNewsAdapter(var list: List<ArticlesItem>,var listener:(model:ArticlesItem)->Unit):RecyclerView.Adapter<CategoryNewsAdapter.VH>() {
    inner class VH(var binding: ItemCategoryNewsRecyclerBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(articlesItem: ArticlesItem) {
            binding.root.setOnClickListener {
                listener(articlesItem)
            }
            Glide.with(binding.root).load(articlesItem.urlToImage).into(binding.image)
            binding.author.setText(articlesItem.author)
            binding.title.setText(articlesItem.title)
            binding.date.setText(articlesItem.publishedAt)
            binding.cardView.startAnimation(AnimationUtils.loadAnimation(binding.root.context, R.anim.recycler_view_anim_bottom))


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemCategoryNewsRecyclerBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}