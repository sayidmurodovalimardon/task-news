package uz.infinityandro.news.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.news.R
import uz.infinityandro.news.data.model.CategoryModel
import uz.infinityandro.news.databinding.CategoryRvItemBinding

class CategoryRvAdapter(
    var list: List<CategoryModel>,
    var itemPosition: Int,
    var listener: (model: CategoryModel) -> Unit

) : RecyclerView.Adapter<CategoryRvAdapter.VH>() {

    inner class VH(var binding: CategoryRvItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(categoryModel: CategoryModel, position: Int) {

            binding.root.setOnClickListener {
                itemPosition = adapterPosition
                listener(categoryModel)
                notifyDataSetChanged()
            }
            binding.categoryText.text = categoryModel.name

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(CategoryRvItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position], position)
        if (position == itemPosition) {

            holder.binding.categoryImage.background = ContextCompat.getDrawable(
                holder.binding.root.context,
                R.drawable.category_selected_card_bg
            )
            holder.binding.categoryText.setTextColor(ContextCompat.getColor(holder.binding.root.context,R.color.white))
        } else {
            holder.binding.categoryImage.background =
                ContextCompat.getDrawable(holder.binding.root.context, R.drawable.category_card_bg)
            holder.binding.categoryText.setTextColor(R.color.black)

        }


    }

    override fun getItemCount(): Int = list.size
}