package uz.infinityandro.worldnews.ui.activity.auth

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.news.databinding.ActivitySignUpBinding
import uz.infinityandro.worldnews.data.AppPreference
import uz.infinityandro.worldnews.presenter.viewmodel.Impl.SignUpViewModelImpl
import uz.infinityandro.worldnews.utils.Constants
import uz.infinityandro.worldnews.utils.displayToast

class SignUpActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySignUpBinding
    private val viewModelImpl: SignUpViewModelImpl by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        listeners()
        observes()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)


    }

    private fun observes() {
        viewModelImpl.connectionLiveData.observe(this, {
            if (it) {
                displayToast("No Internet Connection")
            }
        })
        viewModelImpl.progressLiveData.observe(this, {
            binding.signUpProgress.visibility = if (it) View.VISIBLE else View.GONE
            binding.buttonSignUp.visibility = if (it) View.GONE else View.VISIBLE
        })
        viewModelImpl.errorMessageLiveData.observe(this, {
        })
        viewModelImpl.allDataLiveData.observe(this, {
            if (it) {
                AppPreference.init(applicationContext)
                AppPreference.firstRun=true
                Intent(
                    applicationContext,
                    uz.infinityandro.worldnews.ui.activity.main.MainActivity::class.java
                ).let {
                    startActivity(it)
                    finish()
                }
            }
        })
    }

    private fun listeners() {
        binding.back.setOnClickListener {
            onBackPressed()
        }
        binding.buttonSignUp.setOnClickListener {
            setUsers()
        }
    }

    private fun setUsers() {
        if (binding.inputEmail.text.isNullOrEmpty()) {
            displayToast("Enter your email")
        } else if (binding.inputName.text.isNullOrEmpty()) {
            displayToast("Enter your name")
        } else if (binding.inputPassword.text.isNullOrEmpty()) {
            displayToast("Enter your password")
        } else if (binding.inputPasswordConfirm.text!!.equals(binding.inputPassword.text)) {
            displayToast("Confirm your password")
        } else if (!Patterns.EMAIL_ADDRESS.matcher(binding.inputEmail.text.toString().trim())
                .matches()
        ) {
            displayToast("Enter valid email")
        } else {
            var hashMap = HashMap<String, Any>()
            hashMap.put(Constants.KEY_NAME, binding.inputName.text.toString())
            hashMap.put(Constants.KEY_EMAIL, binding.inputEmail.text.toString())
            hashMap.put(Constants.KEY_PASSWORD, binding.inputPassword.text.toString())
            viewModelImpl.saveUserToFire(hashMap)

        }
    }
}