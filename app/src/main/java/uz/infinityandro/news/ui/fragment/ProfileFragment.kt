package uz.infinityandro.worldnews.ui.fragment

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.firebase.firestore.FirebaseFirestore
import uz.infinityandro.news.R
import uz.infinityandro.news.databinding.ProfileScreenBinding
import uz.infinityandro.worldnews.data.AppPreference
import uz.infinityandro.worldnews.ui.activity.auth.SignInActivity
import uz.infinityandro.worldnews.utils.Constants
import uz.infinityandro.worldnews.utils.isConnected
import uz.infinityandro.worldnews.utils.showToast
import java.io.ByteArrayOutputStream
import java.io.InputStream

class ProfileFragment : Fragment(R.layout.profile_screen) {
    private val binding by viewBinding(ProfileScreenBinding::bind)
    var database = FirebaseFirestore.getInstance()
    lateinit var image: Any


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        AppPreference.init(requireContext())
        setItems()
        listeners()
    }

    private fun listeners() {
        binding.shareApp.setOnClickListener {
            shareApp()
        }
        binding.signOut.setOnClickListener {
            signOut()
        }
        binding.changeModel.setOnClickListener {
            mode()
        }
        binding.headerLayoutImage.setOnClickListener {
            setImage()
        }
        binding.changeHomeNewsCount.setOnClickListener {
            homeChange()
        }
        binding.changeLikeNewsCount.setOnClickListener {
            likeChange()
        }

    }

    private fun likeChange() {
        val builder = AlertDialog.Builder(requireContext()).create()
        val view = layoutInflater.inflate(R.layout.count_home_dialog, binding.root, false)
        builder.setView(view)
        builder.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.setCanceledOnTouchOutside(false)
        val save = view.findViewById<AppCompatTextView>(R.id.save)
        val cancel = view.findViewById<AppCompatTextView>(R.id.cancel)
        val edit = view.findViewById<AppCompatEditText>(R.id.count)

        save.setOnClickListener {
            if (edit.text.isNullOrEmpty() || edit.text!!.equals("0")) {
                showToast("Enter Number")
            } else if (edit.text.toString().toInt() < 1) {
                showToast("Enter a number greater than 1")
            } else if (edit.text.toString().toInt() > 100) {
                showToast("Enter a number greater lower 100")

            } else {
                Constants.CATEGORY_NEWS=edit.text.toString().toInt()
                binding.newsTextCount.text=edit.text.toString()
                builder.dismiss()
            }
        }
        cancel.setOnClickListener {
            builder.dismiss()
        }
        builder.show()

    }

    private fun homeChange() {

        val builder = AlertDialog.Builder(requireContext()).create()
        val view = layoutInflater.inflate(R.layout.count_dialog, binding.root, false)
        builder.setView(view)
        builder.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.setCanceledOnTouchOutside(false)
        val save = view.findViewById<AppCompatTextView>(R.id.save)
        val cancel = view.findViewById<AppCompatTextView>(R.id.cancel)
        val edit = view.findViewById<AppCompatEditText>(R.id.count)

        save.setOnClickListener {
            if (edit.text.isNullOrEmpty() || edit.text!!.equals("0")) {
                showToast("Enter Number")
            } else if (edit.text.toString().toInt() < 20) {
                showToast("Enter a number greater than 20")
            } else if (edit.text.toString().toInt() > 100) {
                showToast("Enter a number greater lower 100")

            } else {
                Constants.RANDOM_NEWS=edit.text.toString().toInt()
                binding.randomTextCount.text=edit.text.toString()
                builder.dismiss()
            }
        }
        cancel.setOnClickListener {
            builder.dismiss()
        }
        builder.show()

    }

    companion object {
        val PICK_IMAGE_REQUEST = 7272
    }


    private fun setImage() {
        val intent = Intent()
        intent.setType("image/*")
        intent.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(
            Intent.createChooser(intent, "Select Picture"),
            PICK_IMAGE_REQUEST
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data != null && data.data != null) {
                val imageUri = data.data!!
                binding.image.setImageURI(imageUri)
                binding.addIcon.visibility = View.GONE
                getInputStreamFromUri(imageUri)?.let { inputStream ->
                    val orginalBitmap = BitmapFactory.decodeStream(inputStream)
                    val width = requireActivity().resources.displayMetrics.widthPixels
                    val height = ((orginalBitmap.height * width) / orginalBitmap.width)
                    val bitmap = Bitmap.createScaledBitmap(orginalBitmap, width, height, false)
                    val outputStream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
                    val byte = outputStream.toByteArray()
                    image = Base64.encodeToString(byte, android.util.Base64.DEFAULT)
                    AppPreference.image= image as String?
                }
            }
        }
    }

    private fun mode() {

    }

    private fun signOut() {
        val builder = AlertDialog.Builder(requireContext()).create()
        builder.setTitle("Sign out")
        builder.setMessage("Do you really want to sign out?")
        builder.setButton(
            AlertDialog.BUTTON_NEGATIVE,
            "Cancel",
            DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })
        builder.setButton(AlertDialog.BUTTON_POSITIVE, "Sign out") { dialog, t ->
            if (isConnected()){
            showToast("Signing out...")
            val document = database.collection(Constants.KEY_COLLECTION_USER)
                .document(AppPreference.userId.toString())
            var hashMap = HashMap<String, Any>()
            document.update(hashMap)
                .addOnSuccessListener {
                    AppPreference.clear()
                    Intent(requireContext(), SignInActivity::class.java).let {
                        startActivity(it)
                    }
                    activity?.finish()
                }.addOnFailureListener {
                    showToast("Unable to Sign out")
                }
            }else{
                showToast("No Internet Connection")

            }
            dialog.dismiss()


        }
        builder.setOnShowListener {
            builder.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(android.R.color.holo_red_dark))
            builder.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(android.R.color.black))
        }
        builder.show()


    }

    private fun shareApp() {
        val dialog = android.app.AlertDialog.Builder(requireContext()).create()
        dialog.setTitle("Do you want to share this app?")
        dialog.setCanceledOnTouchOutside(false)
        dialog.setButton(
            android.app.AlertDialog.BUTTON_POSITIVE,
            "Yes",
            DialogInterface.OnClickListener { dialog, which ->
                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "text/plain"
                val url = ""
                intent.putExtra(Intent.EXTRA_SUBJECT, "Wallpaper App")
                intent.putExtra(Intent.EXTRA_TEXT, url)
                startActivity(intent)
            })
        dialog.setButton(
            android.app.AlertDialog.BUTTON_NEGATIVE,
            "No",
            DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })
        dialog.show()


    }
    private fun getInputStreamFromUri(uri: Uri): InputStream? {
        return requireActivity().contentResolver.openInputStream(uri)
    }

    private fun setItems() {
        binding.email.text=AppPreference.email
        binding.newsTextCount.text = Constants.CATEGORY_NEWS.toString()
        binding.randomTextCount.text = Constants.RANDOM_NEWS.toString()
        if (!AppPreference.image.isNullOrEmpty()) {
            val byte = Base64.decode(AppPreference.image, Base64.DEFAULT)
            val bitmap = BitmapFactory.decodeByteArray(byte, 0, byte.size)
            binding.image.setImageBitmap(bitmap)
            binding.addIcon.visibility = View.GONE
        }

    }
}