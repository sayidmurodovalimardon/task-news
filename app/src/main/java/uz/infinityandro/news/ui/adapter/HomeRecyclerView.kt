package uz.infinityandro.worldnews.ui.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import uz.infinityandro.news.databinding.ItemHomeBinding
import uz.infinityandro.worldnews.data.model.ArticlesItem
import uz.infinityandro.worldnews.utils.isConnected

class HomeRecyclerView(var context: Context,var list:List<ArticlesItem>,var listener:(model:ArticlesItem)->Unit):RecyclerView.Adapter<HomeRecyclerView.VH>() {
    inner class VH(val binding: ItemHomeBinding):RecyclerView.ViewHolder(binding.root){

        fun bind(articlesItem: ArticlesItem) {
            binding.root.setOnClickListener {
                listener(articlesItem)
            }

            Glide.with(context).load(articlesItem.urlToImage).centerCrop().listener(object :RequestListener<Drawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return true
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.progress.visibility=View.GONE
                    return false
                }
            }).into(binding.imageNews)

            binding.author.setText(articlesItem.author)
            binding.category.setText(articlesItem.source?.name)
            binding.title.setText(articlesItem.title)


        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemHomeBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}