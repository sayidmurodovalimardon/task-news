package uz.infinityandro.news.ui.activity.main

import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import uz.infinityandro.news.R
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.news.databinding.ActivityNewsBinding
import uz.infinityandro.worldnews.data.model.ArticlesItem
import uz.infinityandro.worldnews.utils.isConnected

class NewsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNewsBinding
    lateinit var news:Any
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (isConnected()){
         news=intent.getSerializableExtra("new") as ArticlesItem
            setItem(news as ArticlesItem)
            listeners(news as ArticlesItem)
        }else{
            news=intent.getSerializableExtra("new") as Article
            setItemNews(news as Article)
            listenersNews(news as Article)
        }

    }

    private fun listenersNews(news: Article) {
        binding.blurBack.setOnClickListener{
            onBackPressed()
        }
        binding.website.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(news.url)
            startActivity(intent)
        }
    }

    private fun setItemNews(news: Article) {
        Glide.with(applicationContext).load(news.urlToImage).centerCrop().listener(object :RequestListener<Drawable>{
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                return true
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                binding.progressImage.visibility= View.GONE

                return false
            }

        }).into(binding.imageBg)
        binding.author.setText("Publishe by ${news.author}")
        binding.content.setText("${ news.content } ${R.string.qoshimcha}")
        binding.date.setText("Date: ${news.publishedAt}")
        binding.description.setText(news.description)
        binding.boshqa.setText(R.string.qoshimcha)
        binding.title.setText(news.title)
    }

    private fun listeners(news: ArticlesItem) {
        binding.blurBack.setOnClickListener{
            onBackPressed()
        }
        binding.website.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(news.url)
            startActivity(intent)
        }
    }

    private fun setItem(news: ArticlesItem) {
        Glide.with(applicationContext).load(news.urlToImage).centerCrop().listener(object :RequestListener<Drawable>{
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                return true
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                binding.progressImage.visibility= View.GONE

                return false
            }

        }).into(binding.imageBg)
        binding.author.setText("Publishe by ${news.author}")
        binding.content.setText("${ news.content } and ${R.string.qoshimcha}")
        binding.boshqa.setText(R.string.qoshimcha)
        binding.date.setText("Date: ${news.publishedAt}")
        binding.description.setText(news.description)
        binding.title.setText(news.title)
    }

    override fun onStart() {
        super.onStart()
        binding.blurTitle.startBlur()
    }

    override fun onDestroy() {
        binding.blurTitle.pauseBlur()
        super.onDestroy()

    }
}