package uz.infinityandro.news.ui.adapter

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.news.databinding.ItemHomeBinding

class GetDaoAdapter(var context: Context, var list:List<Article>, var listener:(model: Article)->Unit):RecyclerView.Adapter<GetDaoAdapter.VH>() {
    inner class VH(var binding:ItemHomeBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(newsSadeModel: Article) {
            binding.root.setOnClickListener {
                listener(newsSadeModel)
            }
            Glide.with(context).load(newsSadeModel.urlToImage).centerCrop().listener(object :
                RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return true
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.progress.visibility= View.GONE
                    return false
                }
            }).into(binding.imageNews)
            binding.author.setText(newsSadeModel.author)
            binding.category.setText(newsSadeModel.name)
            binding.title.setText(newsSadeModel.title)}
        }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemHomeBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])

    }

    override fun getItemCount(): Int {
        return list.size

    }
}