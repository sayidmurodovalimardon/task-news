package uz.infinityandro.worldnews.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.news.R
import uz.infinityandro.news.databinding.NewsScreenBinding
import uz.infinityandro.news.presenter.viewmodel.Impl.NewsFragmentViewModelImpl
import uz.infinityandro.news.ui.activity.main.NewsActivity
import uz.infinityandro.news.ui.adapter.CategoryNewsAdapter
import uz.infinityandro.news.ui.adapter.CategoryRvAdapter
import uz.infinityandro.worldnews.data.model.ArticlesItem
import uz.infinityandro.worldnews.utils.Constants
import uz.infinityandro.worldnews.utils.showToast

class NewsFragment : Fragment(R.layout.news_screen) {
    private val binding by viewBinding(NewsScreenBinding::bind)
    private lateinit var categoryRvAdapter: CategoryRvAdapter
    private lateinit var categoryNewsAdapter: CategoryNewsAdapter
    private val viewModel: NewsFragmentViewModelImpl by viewModel()
    var positionAdapter = 0
    var list = ArrayList<ArticlesItem>()
    var category = "business"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        adapters()
        observers()
        getNewsModel()
    }

    private fun getNewsModel() {
        viewModel.getAllBooks(category, Constants.CATEGORY_NEWS, Constants.PAGE, Constants.ACCESS_KEY)
    }

    private fun observers() {
        viewModel.connectionLiveData.observe(requireActivity(), {

        })
        viewModel.progressLiveData.observe(requireActivity(), {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE

            }
        })
        viewModel.errorMessageLiveData.observe(requireActivity(), {
            showToast(
                it
            )
        })
        viewModel.allDataLiveData.observe(requireActivity(), {
            if (!it.articles.isNullOrEmpty()) {
                list.addAll(it.articles)
                categoryNewsAdapter.notifyDataSetChanged()
            }
        })

    }

    private fun adapters() {
        categoryRvAdapter = CategoryRvAdapter(Constants.getCategories(), positionAdapter) {
            list.clear()
            category = it.name
            viewModel.getAllBooks(category, Constants.CATEGORY_NEWS, Constants.PAGE, Constants.ACCESS_KEY)
            binding.progressBar.visibility=View.VISIBLE
        }
        binding.categoryRecycler.adapter = categoryRvAdapter

        categoryNewsAdapter = CategoryNewsAdapter(list) { news ->
            Intent(requireContext(), NewsActivity::class.java).let {
                it.putExtra("new", news)
                startActivity(it)
            }
        }
        binding.newsRecycler.adapter = categoryNewsAdapter

    }
}