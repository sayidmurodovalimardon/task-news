package uz.infinityandro.news.presenter.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import uz.infinityandro.news.data.model.Article

@Dao
interface NewsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: ArrayList<Article>)

    @Query("select * from article")
    fun getNews():List<Article>
}