package uz.infinityandro.news.presenter.repository

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.worldnews.data.model.NewsData

interface NewsFragmentRepository {
    fun getNews(q:String,pageSize:Int,page:Int,apiKey:String): Flow<Result<NewsData>>

}