package uz.infinityandro.worldnews.presenter.repository

import androidx.lifecycle.LiveData
import kotlinx.coroutines.flow.Flow
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.worldnews.data.model.ArticlesItem
import uz.infinityandro.worldnews.data.model.NewsData

interface NewsRepository {
    fun getAllNews(country:String,page:Int,pageSize:Int,apiKey:String):Flow<Result<NewsData>>

}