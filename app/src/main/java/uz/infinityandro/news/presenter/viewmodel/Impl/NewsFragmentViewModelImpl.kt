package uz.infinityandro.news.presenter.viewmodel.Impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.news.presenter.repository.NewsFragmentRepository
import uz.infinityandro.news.presenter.viewmodel.NewsFragmentViewModel
import uz.infinityandro.worldnews.data.model.NewsData
import uz.infinityandro.worldnews.utils.isConnected

class NewsFragmentViewModelImpl(private val repository: NewsFragmentRepository):ViewModel(),NewsFragmentViewModel {
    override val errorMessageLiveData= MutableLiveData<String>()
    override val connectionLiveData= MutableLiveData<Boolean>()
    override val allDataLiveData= MutableLiveData<NewsData>()
    override val progressLiveData= MutableLiveData<Boolean>()

    override fun getAllBooks(q: String, pageSize: Int, page: Int, apiKey: String) {
        if (!isConnected()){
            connectionLiveData.postValue(true)
            return
        }
        connectionLiveData.postValue(false)
        repository.getNews(q, pageSize, page, apiKey).onEach {
            progressLiveData.postValue(true)
            it.onSuccess { news->
                progressLiveData.postValue(false)
                allDataLiveData.postValue(news)
            }.onFailure {
                errorMessageLiveData.postValue(it.localizedMessage)
            }
        }.launchIn(viewModelScope)

    }
}