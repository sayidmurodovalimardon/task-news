package uz.infinityandro.news.presenter.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.worldnews.data.model.ArticlesItem

@Database(entities = [Article::class],version = 1,exportSchema = false)
abstract class NewsDatabase :RoomDatabase(){
    abstract fun newsDao():NewsDao

    companion object{
        private var instance:NewsDatabase?=null

        fun getInstance(context: Context):NewsDatabase{
            if (instance==null){
            synchronized(this){

                    instance=DatabaseSave(context)

            }
            }
            return instance!!
        }

        private fun DatabaseSave(context: Context): NewsDatabase? =
            Room.databaseBuilder(context.applicationContext,NewsDatabase::class.java,"news_do")
                .build()
    }

}