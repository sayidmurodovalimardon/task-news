package uz.infinityandro.worldnews.presenter.viewmodel.Impl

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.worldnews.presenter.repository.SignUpRepository
import uz.infinityandro.worldnews.presenter.viewmodel.SignUpViewModel
import uz.infinityandro.worldnews.utils.isConnected

class SignUpViewModelImpl(private val signUpRepository: SignUpRepository) : ViewModel(),
    SignUpViewModel {
    override val errorMessageLiveData = MutableLiveData<String>()
    override val connectionLiveData = MutableLiveData<Boolean>()
    override val allDataLiveData = MutableLiveData<Boolean>()
    override val progressLiveData = MutableLiveData<Boolean>()

    override fun saveUserToFire(user: HashMap<String, Any>) {
        if (!isConnected()) {
            connectionLiveData.postValue(true)
            return
        }
        connectionLiveData.postValue(false)
        signUpRepository.saveDataToFireStore(user).onEach {
            progressLiveData.postValue(true)

            it.onSuccess {
                allDataLiveData.postValue(true)
            }.onFailure {
                errorMessageLiveData.postValue(it.message)
            }
        }.launchIn(viewModelScope)

    }
}