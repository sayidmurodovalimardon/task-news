package uz.infinityandro.news.presenter.viewmodel.Impl

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.news.presenter.repository.NewsGetDao
import uz.infinityandro.news.presenter.viewmodel.NewsDatabaseViewModel
import uz.infinityandro.worldnews.data.model.ArticlesItem
import uz.infinityandro.worldnews.utils.isConnected

class NewsDatabaseViewModelImpl(private val newsGetDao: NewsGetDao):ViewModel(),NewsDatabaseViewModel {
    override val errorMessageLiveData= MutableLiveData<String>()
    override val connectionLiveData= MutableLiveData<Boolean>()
    override val allDataLiveData= MutableLiveData<List<Article>>()
    override val progressLiveData= MutableLiveData<Boolean>()

    override fun getAllBooks() {
        if (!isConnected()){
            connectionLiveData.postValue(true)
            return
        }
        connectionLiveData.postValue(false)
        newsGetDao.getNews().onEach {
            progressLiveData.postValue(true)

            it.onSuccess {
                progressLiveData.postValue(false)
                allDataLiveData.postValue(it)
            }.onFailure {
                errorMessageLiveData.postValue(it.message)
            }
        }
    }
}