package uz.infinityandro.news.presenter.repository

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.worldnews.data.model.ArticlesItem

interface NewsGetDao {
    fun getNews():Flow<Result<List<Article>>>
}