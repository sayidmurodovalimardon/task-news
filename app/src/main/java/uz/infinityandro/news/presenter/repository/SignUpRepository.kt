package uz.infinityandro.worldnews.presenter.repository

import kotlinx.coroutines.flow.Flow

interface SignUpRepository {
    fun saveDataToFireStore(user:HashMap<String,Any>):Flow<Result<Boolean>>
}