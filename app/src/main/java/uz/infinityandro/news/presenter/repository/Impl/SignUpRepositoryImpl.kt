package uz.infinityandro.worldnews.presenter.repository.Impl

import android.content.Context
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import uz.infinityandro.worldnews.data.AppPreference
import uz.infinityandro.worldnews.presenter.repository.SignUpRepository
import uz.infinityandro.worldnews.utils.Constants

class SignUpRepositoryImpl(private val context: Context) : SignUpRepository {

    var database = FirebaseFirestore.getInstance()

    override fun saveDataToFireStore(user: HashMap<String, Any>): Flow<Result<Boolean>> = flow {
        var a = false
        var error = ""
        database.collection(Constants.KEY_COLLECTION_USER)
            .add(user)
            .addOnSuccessListener {
                AppPreference.init(context)
                AppPreference.name = user[Constants.KEY_NAME].toString()
                AppPreference.email=user[Constants.KEY_EMAIL].toString()
                AppPreference.userId=it.id
                a = true
            }.addOnFailureListener {
                error = it.localizedMessage
            }
        emit(Result.success(a))
        emit(Result.failure(Throwable(error)))

    }.flowOn(Dispatchers.IO)
}