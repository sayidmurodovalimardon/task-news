package uz.infinityandro.news.presenter.viewmodel

import androidx.lifecycle.LiveData
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.worldnews.data.model.ArticlesItem

interface NewsDatabaseViewModel {
    val errorMessageLiveData: LiveData<String>
    val connectionLiveData: LiveData<Boolean>
    val allDataLiveData: LiveData<List<Article>>
    val progressLiveData: LiveData<Boolean>

    fun getAllBooks()

}