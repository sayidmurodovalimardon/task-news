package uz.infinityandro.worldnews.presenter.repository.Impl

import android.content.Context
import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.news.presenter.room.NewsDao
import uz.infinityandro.worldnews.data.model.ArticlesItem
import uz.infinityandro.worldnews.data.model.NewsData
import uz.infinityandro.worldnews.presenter.api.ApiClient
import uz.infinityandro.worldnews.presenter.api.ApiService
import uz.infinityandro.worldnews.presenter.repository.NewsRepository

class NewsRepositoryImpl(private val context: Context) :
    NewsRepository {
    override fun getAllNews(
        country: String,
        pageSize: Int,
        page: Int,
        apiKey: String
    ): Flow<Result<NewsData>> = flow {
        val api = ApiClient.getNews().create(ApiService::class.java)
        val response = api.getHomeNews(country, page, pageSize, apiKey)
        if (response.isSuccessful) {
            emit(Result.success(response.body()!!))

        } else {
            emit(Result.failure(Throwable(response.message())))
        }
    }.flowOn(Dispatchers.IO)

}