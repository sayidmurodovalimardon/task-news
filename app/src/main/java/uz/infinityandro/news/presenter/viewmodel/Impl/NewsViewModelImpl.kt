package uz.infinityandro.worldnews.presenter.viewmodel.Impl

import android.app.Application
import android.content.Context
import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.infinityandro.news.data.model.Article
import uz.infinityandro.news.presenter.room.NewsDatabase
import uz.infinityandro.worldnews.data.model.NewsData
import uz.infinityandro.worldnews.presenter.repository.NewsRepository
import uz.infinityandro.worldnews.presenter.viewmodel.NewsViewModel
import uz.infinityandro.worldnews.utils.isConnected

class NewsViewModelImpl(private val repository: NewsRepository):ViewModel(),NewsViewModel {
    override val errorMessageLiveData= MutableLiveData<String>()
    override val connectionLiveData= MutableLiveData<Boolean>()
    override val allDataLiveData= MutableLiveData<NewsData>()
    override val allDataLiveDataNoInternet= MutableLiveData<NewsData>()
    override val progressLiveData= MutableLiveData<Boolean>()

    override fun getAllBooks(context: Context,q: String,page: Int,pageSize: Int, apiKey: String) {
        if (!isConnected()){
            connectionLiveData.postValue(true)
            return
        }
        progressLiveData.postValue(true)
        repository.getAllNews(q, pageSize, page, apiKey).onEach {
            progressLiveData.postValue(true)
            it.onSuccess { news->
                progressLiveData.postValue(false)
                allDataLiveData.postValue(news)

            }.onFailure {error->
                errorMessageLiveData.postValue(error.toString())
            }
        }.launchIn(viewModelScope)
    }
}