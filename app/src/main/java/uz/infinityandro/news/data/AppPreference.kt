package uz.infinityandro.worldnews.data

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import uz.infinityandro.worldnews.utils.Constants

object AppPreference {
    private const val NAME = "news"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor:SharedPreferences.Editor = edit()
        operation(editor)
        editor.apply()
    }


    var firstRun: Boolean
        get() = preferences.getBoolean(Constants.KEY_IS_SIGNED_IN, false)
        set(value) = preferences.edit {
            it.putBoolean(
                Constants.KEY_IS_SIGNED_IN, value
            )
        }

    var name: String?
        get() = preferences.getString(Constants.KEY_NAME,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_NAME, value
            )
        }
    var email: String?
        get() = preferences.getString(Constants.KEY_EMAIL,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_NAME, value
            )
        }
    var userId: String?
        get() = preferences.getString(Constants.KEY_USER_ID,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_USER_ID, value
            )
        }
    var image: String?
        get() = preferences.getString(Constants.KEY_IMAGE,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_IMAGE, value
            )
        }

    @SuppressLint("ApplySharedPref")
    fun clear(){
        preferences.edit().remove(NAME).commit()
        preferences.edit().clear().apply()
    }

}