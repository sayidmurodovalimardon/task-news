package uz.infinityandro.worldnews.coin

import org.koin.dsl.module
import uz.infinityandro.news.presenter.repository.Impl.NewsFragmentRepositoryImpl
import uz.infinityandro.news.presenter.repository.Impl.NewsGetDaoImpl
import uz.infinityandro.news.presenter.repository.NewsFragmentRepository
import uz.infinityandro.news.presenter.repository.NewsGetDao
import uz.infinityandro.worldnews.presenter.repository.Impl.NewsRepositoryImpl
import uz.infinityandro.worldnews.presenter.repository.Impl.SignUpRepositoryImpl
import uz.infinityandro.worldnews.presenter.repository.NewsRepository
import uz.infinityandro.worldnews.presenter.repository.SignUpRepository

val repositoryModule= module {
    factory <SignUpRepository>{ SignUpRepositoryImpl(context = get()) }
    factory <NewsRepository>{ NewsRepositoryImpl(context = get())}
    factory <NewsFragmentRepository> { NewsFragmentRepositoryImpl() }
    factory <NewsGetDao>{ NewsGetDaoImpl(context = get()) }
}