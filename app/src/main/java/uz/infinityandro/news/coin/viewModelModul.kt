package uz.infinityandro.worldnews.coin

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import uz.infinityandro.news.presenter.viewmodel.Impl.NewsDatabaseViewModelImpl
import uz.infinityandro.news.presenter.viewmodel.Impl.NewsFragmentViewModelImpl
import uz.infinityandro.worldnews.presenter.viewmodel.Impl.NewsViewModelImpl
import uz.infinityandro.worldnews.presenter.viewmodel.Impl.SignUpViewModelImpl

val viewModelModul = module {
    viewModel { SignUpViewModelImpl(signUpRepository = get()) }
    viewModel { NewsViewModelImpl(repository = get()) }
    viewModel { NewsFragmentViewModelImpl(repository = get()) }
    viewModel { NewsDatabaseViewModelImpl(newsGetDao = get()) }
}