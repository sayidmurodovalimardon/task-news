package uz.infinityandro.worldnews.utils

import uz.infinityandro.news.data.model.CategoryModel

object Constants {
    const val KEY_COLLECTION_USER = "user"
    const val KEY_IS_SIGNED_IN = "isSIgnedIn"
    const val KEY_NAME = "name"
    const val KEY_IMAGE = "image"
    const val KEY_EMAIL = "email"
    const val KEY_PASSWORD = "password"
    const val KEY_USER_ID="userId"


    //    const val ACCESS_KEY="ae5b17ed05054689b578c815e9600bdd"
    const val ACCESS_KEY = "12569ee8a1ca4606ada91b01ab4d96bf"
    const val BASE_URL = "https://newsapi.org"

    fun getCategories(): ArrayList<CategoryModel> {
        val list = ArrayList<CategoryModel>()
        list.add(CategoryModel("business"))
        list.add(CategoryModel("entertainment"))
        list.add(CategoryModel("general"))
        list.add(CategoryModel("health"))
        list.add(CategoryModel("science"))
        list.add(CategoryModel("sports"))
        list.add(CategoryModel("technology"))
        return list
    }
    var COUNTRY="us"
    var PAGE=1
    // min qiymati 20 max 100
    var RANDOM_NEWS = 20
    //min 1 max 100
    var CATEGORY_NEWS=10

}